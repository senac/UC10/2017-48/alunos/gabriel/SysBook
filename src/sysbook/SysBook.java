package sysbook;

import br.com.senac.view.JFramePrincipal;
import java.util.*;
import br.com.senac.model.cliente.*;
import br.com.senac.model.emprestimo.Emprestimo;
import br.com.senac.model.livro.*;



public class SysBook {
    
    public static List<Cliente> listaCliente = new ArrayList<Cliente>();
    public static List<Autor> listaAutor = new ArrayList<>();
    public static List<Genero> listaGenero = new ArrayList<>();
    public static List<Livro> listaLivro = new ArrayList<>();
    public static List<Emprestimo> listaEmprestimo = new ArrayList<Emprestimo>();
    
    public static void main(String[] args) {
        new JFramePrincipal();
        
    }
}
