package br.com.senac.model.livro;

import java.util.ArrayList;
import java.util.List;

public class Livro {
    
    private String titulo;
    private int isbn;
    private int anoEdicao;
    private String editora;
    
    public static List<Autor> listaAutorLivro = new ArrayList<>();
    public static List<Genero> listaGeneroLivro = new ArrayList<>();

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getIsbn() {
        return isbn;
    }

    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }

    public int getAnoEdicao() {
        return anoEdicao;
    }

    public void setAnoEdicao(int anoEdicao) {
        this.anoEdicao = anoEdicao;
    }

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public void adicionarAutor(List<Autor> lista){
        this.listaAutorLivro = lista;
    }
    
    public void adicionarGenero(List<Genero> lista){
        this.listaGeneroLivro = lista;
    }
    
    @Override
    public String toString() {
        return this.titulo + " - " + this.listaAutorLivro;
    }
    
    
}
