package br.com.senac.model.emprestimo;

import br.com.senac.model.cliente.Cliente;
import br.com.senac.model.livro.Livro;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Emprestimo {
    
    private Date dataDevolucao;
    private Date dataEmprestimo = new Date();
    private List<Livro> livrosAlugados = new ArrayList<>();
    private Cliente cliente;

    public Date getDataDevolucao() {
        return dataEmprestimo;
    }

    public void setDataDevolucao(Date dataDevolucao) {
        this.dataDevolucao = dataDevolucao;
    }

    public Date getDataEmprestimo() {
        return dataEmprestimo;
    }

    public void setDataEmprestimo(Date dataEmprestimo) {
        this.dataEmprestimo = dataEmprestimo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
          
    public void adicionarLivro(List<Livro> lista){
        this.livrosAlugados = lista;
    }

    @Override
    public String toString() {
        return "Emprestimo{" + "dataDevolucao=" + dataDevolucao + ", dataEmprestimo=" + dataEmprestimo + ", livrosAlugados=" + livrosAlugados + ", cliente=" + cliente + '}';
    }

    
}
