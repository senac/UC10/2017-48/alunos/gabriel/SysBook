
package br.com.senac.view.adicionar;

import br.com.senac.model.livro.Autor;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import sysbook.SysBook;
import br.com.senac.view.cadastro.JFrameLivroCadastro;
import java.util.ArrayList;
import java.util.List;


public class JFrameAutorAdicionar extends javax.swing.JFrame {

    private JFrameLivroCadastro jFrameLivroCadastro;
    
    public JFrameAutorAdicionar() {
        initComponents();
        this.setVisible(true);
        
        preencherLista();
    }
    
    public JFrameAutorAdicionar(JFrameLivroCadastro jFrameLivroCadastro){
        this();
        this.jFrameLivroCadastro = jFrameLivroCadastro;
    }
    
    private void preencherLista(){
        DefaultListModel<Autor> defaultListModel = new DefaultListModel<>();
        
        for(Autor a : SysBook.listaAutor){
            defaultListModel.addElement(a);
        }
        
        this.jListAutor.setModel(defaultListModel);
    }
    private void captura(){
        Autor autor = (Autor)this.jListAutor.getSelectedValue();
        
        jFrameLivroCadastro.pegarClasse(autor);
        
        jFrameLivroCadastro.prenecherListaAutor();
        
        this.dispose();
    }

  
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListAutor = new javax.swing.JList();
        jButtonCancelarAutorAdicionar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel1.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jLabel1.setText("Adicionar Autor");

        jListAutor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jListAutorMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jListAutor);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel1))
                .addContainerGap(29, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        jButtonCancelarAutorAdicionar.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jButtonCancelarAutorAdicionar.setText("Cancelar");
        jButtonCancelarAutorAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarAutorAdicionarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(196, 196, 196)
                        .addComponent(jButtonCancelarAutorAdicionar))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButtonCancelarAutorAdicionar, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCancelarAutorAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarAutorAdicionarActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButtonCancelarAutorAdicionarActionPerformed

    private void jListAutorMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jListAutorMouseClicked
        if(evt.getClickCount() > 1){
            captura();
        }
    }//GEN-LAST:event_jListAutorMouseClicked

   
  

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCancelarAutorAdicionar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JList jListAutor;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
