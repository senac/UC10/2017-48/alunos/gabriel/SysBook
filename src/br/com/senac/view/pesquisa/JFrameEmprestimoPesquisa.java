package br.com.senac.view.pesquisa;

import br.com.senac.model.emprestimo.Emprestimo;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;
import sysbook.SysBook;

public class JFrameEmprestimoPesquisa extends javax.swing.JFrame {

    public JFrameEmprestimoPesquisa() {
        initComponents();
        this.setVisible(true);
        
        preencherLista(SysBook.listaEmprestimo);
    }
    
    private void preencherLista(List<Emprestimo> lista) {
        DefaultListModel<Emprestimo> defaultListModel = new DefaultListModel<Emprestimo>();
        
        for(Emprestimo e : lista){
            defaultListModel.addElement(e);
        }
        
        this.jListEmprestimoPesquisa.setModel(defaultListModel);
    }
   

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldEmprestimoPesquisa = new javax.swing.JTextField();
        jButtonEmprestimoPesquisar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListEmprestimoPesquisa = new javax.swing.JList();
        jButtonCancelarEmprestimoPesquisa = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel1.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jLabel1.setText("Pesquisar Livro");

        jButtonEmprestimoPesquisar.setText("Pesquisar");
        jButtonEmprestimoPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEmprestimoPesquisarActionPerformed(evt);
            }
        });

        jScrollPane1.setViewportView(jListEmprestimoPesquisa);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 201, Short.MAX_VALUE)
                            .addComponent(jTextFieldEmprestimoPesquisa))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonEmprestimoPesquisar))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldEmprestimoPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonEmprestimoPesquisar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(22, Short.MAX_VALUE))
        );

        jButtonCancelarEmprestimoPesquisa.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jButtonCancelarEmprestimoPesquisa.setText("Cancelar");
        jButtonCancelarEmprestimoPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarEmprestimoPesquisaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(238, Short.MAX_VALUE)
                .addComponent(jButtonCancelarEmprestimoPesquisa)
                .addGap(33, 33, 33))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonCancelarEmprestimoPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCancelarEmprestimoPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarEmprestimoPesquisaActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButtonCancelarEmprestimoPesquisaActionPerformed

    private void jButtonEmprestimoPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEmprestimoPesquisarActionPerformed
        String nome = this.jTextFieldEmprestimoPesquisa.getText();
        
        if(!nome.equals("")){
            List<Emprestimo> listaFiltrada = new ArrayList<>();
            
            for(Emprestimo e : SysBook.listaEmprestimo){
                if(e.getCliente().getNomeCliente().equals(nome)){
                 listaFiltrada.add(e);
                }
            }
            this.preencherLista(listaFiltrada);
        }
        else{
            this.preencherLista(SysBook.listaEmprestimo);
        }
    }//GEN-LAST:event_jButtonEmprestimoPesquisarActionPerformed

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCancelarEmprestimoPesquisa;
    private javax.swing.JButton jButtonEmprestimoPesquisar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JList jListEmprestimoPesquisa;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextFieldEmprestimoPesquisa;
    // End of variables declaration//GEN-END:variables

}
