
package br.com.senac.view;
import br.com.senac.view.cadastro.*;
import br.com.senac.view.pesquisa.*;
import br.com.senac.view.emprestimo.*;

public class JFramePrincipal extends javax.swing.JFrame {

    
    public JFramePrincipal() {
        initComponents();
        this.setVisible(true);
    }

    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenuCadastro = new javax.swing.JMenu();
        jMenuItemClienteCadastro = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItemLivroCadastro = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jMenuItemAutorCadastro = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        jMenuItemGeneroCadastro = new javax.swing.JMenuItem();
        jMenuPesquisa = new javax.swing.JMenu();
        jMenuItemClientePesquisa = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        jMenuItemLivroPesquisa = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        jMenuItemEmprestimoPesquisa = new javax.swing.JMenuItem();
        jMenuAjuda = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jButton1.setFont(new java.awt.Font("sansserif", 0, 18)); // NOI18N
        jButton1.setText("Emprestimo");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(69, Short.MAX_VALUE)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(60, 60, 60))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(30, Short.MAX_VALUE)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );

        jMenuCadastro.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jMenuCadastro.setText("Cadastro");

        jMenuItemClienteCadastro.setText("Cliente");
        jMenuItemClienteCadastro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemClienteCadastroActionPerformed(evt);
            }
        });
        jMenuCadastro.add(jMenuItemClienteCadastro);
        jMenuCadastro.add(jSeparator1);

        jMenuItemLivroCadastro.setText("Livro");
        jMenuItemLivroCadastro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemLivroCadastroActionPerformed(evt);
            }
        });
        jMenuCadastro.add(jMenuItemLivroCadastro);
        jMenuCadastro.add(jSeparator2);

        jMenuItemAutorCadastro.setText("Autor");
        jMenuItemAutorCadastro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemAutorCadastroActionPerformed(evt);
            }
        });
        jMenuCadastro.add(jMenuItemAutorCadastro);
        jMenuCadastro.add(jSeparator3);

        jMenuItemGeneroCadastro.setText("Gênero");
        jMenuItemGeneroCadastro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemGeneroCadastroActionPerformed(evt);
            }
        });
        jMenuCadastro.add(jMenuItemGeneroCadastro);

        jMenuBar1.add(jMenuCadastro);

        jMenuPesquisa.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jMenuPesquisa.setText("Pesquisa");

        jMenuItemClientePesquisa.setText("Cliente");
        jMenuItemClientePesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemClientePesquisaActionPerformed(evt);
            }
        });
        jMenuPesquisa.add(jMenuItemClientePesquisa);
        jMenuPesquisa.add(jSeparator4);

        jMenuItemLivroPesquisa.setText("Livro");
        jMenuItemLivroPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemLivroPesquisaActionPerformed(evt);
            }
        });
        jMenuPesquisa.add(jMenuItemLivroPesquisa);
        jMenuPesquisa.add(jSeparator5);

        jMenuItemEmprestimoPesquisa.setText("Emprestimos");
        jMenuItemEmprestimoPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemEmprestimoPesquisaActionPerformed(evt);
            }
        });
        jMenuPesquisa.add(jMenuItemEmprestimoPesquisa);

        jMenuBar1.add(jMenuPesquisa);

        jMenuAjuda.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jMenuAjuda.setText("Ajuda");
        jMenuBar1.add(jMenuAjuda);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(41, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(156, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItemClienteCadastroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemClienteCadastroActionPerformed
        new JFrameClienteCadastro();
    }//GEN-LAST:event_jMenuItemClienteCadastroActionPerformed

    private void jMenuItemLivroCadastroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemLivroCadastroActionPerformed
        new JFrameLivroCadastro();
    }//GEN-LAST:event_jMenuItemLivroCadastroActionPerformed

    private void jMenuItemGeneroCadastroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemGeneroCadastroActionPerformed
        new JFrameGeneroCadastro();
    }//GEN-LAST:event_jMenuItemGeneroCadastroActionPerformed

    private void jMenuItemAutorCadastroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemAutorCadastroActionPerformed
        new JFrameAutorCadastro();
    }//GEN-LAST:event_jMenuItemAutorCadastroActionPerformed

    private void jMenuItemClientePesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemClientePesquisaActionPerformed
        new JFrameClientePesquisa();
    }//GEN-LAST:event_jMenuItemClientePesquisaActionPerformed

    private void jMenuItemLivroPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemLivroPesquisaActionPerformed
        new JFrameLivroPesquisa();
    }//GEN-LAST:event_jMenuItemLivroPesquisaActionPerformed

    private void jMenuItemEmprestimoPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemEmprestimoPesquisaActionPerformed
        new JFrameEmprestimoPesquisa();
    }//GEN-LAST:event_jMenuItemEmprestimoPesquisaActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        new JFrameEmprestimo();
    }//GEN-LAST:event_jButton1ActionPerformed

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JMenu jMenuAjuda;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenu jMenuCadastro;
    private javax.swing.JMenuItem jMenuItemAutorCadastro;
    private javax.swing.JMenuItem jMenuItemClienteCadastro;
    private javax.swing.JMenuItem jMenuItemClientePesquisa;
    private javax.swing.JMenuItem jMenuItemEmprestimoPesquisa;
    private javax.swing.JMenuItem jMenuItemGeneroCadastro;
    private javax.swing.JMenuItem jMenuItemLivroCadastro;
    private javax.swing.JMenuItem jMenuItemLivroPesquisa;
    private javax.swing.JMenu jMenuPesquisa;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    // End of variables declaration//GEN-END:variables
}
