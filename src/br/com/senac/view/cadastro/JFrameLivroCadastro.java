package br.com.senac.view.cadastro;

import br.com.senac.model.livro.Autor;
import br.com.senac.model.livro.Genero;
import br.com.senac.model.livro.Livro;
import br.com.senac.view.adicionar.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import sysbook.SysBook;

public class JFrameLivroCadastro extends javax.swing.JFrame {

    private Livro livro = new Livro();
    public static List<Autor> listaAutor = new ArrayList<>();
    public static List<Genero> listaGenero = new ArrayList<>();
    
    public JFrameLivroCadastro() {
        initComponents();
        this.setVisible(true);
    }

    private void salvar() {

        livro.setTitulo(this.jTextFieldTituloCadastro.getText());
        livro.setIsbn(Integer.valueOf(this.jFormattedTextFieldISBNCadastro1.getText()));
        livro.setAnoEdicao(Integer.valueOf(this.jFormattedTextFieldAnoEdicaoCadastro.getText()));
        livro.setEditora(this.jTextFieldEditoraCadastro.getText());
        livro.adicionarAutor(listaAutor);
        livro.adicionarGenero(listaGenero);
        
        SysBook.listaLivro.add(livro);

        JOptionPane.showMessageDialog(this, "Livro Salvo", "Sucesso", JOptionPane.INFORMATION_MESSAGE);

        this.dispose();
    }
    
    public void prenecherListaAutor(){
        DefaultListModel<Autor> defaultListModel = new DefaultListModel<>();
        
        for(Autor a : listaAutor){
            defaultListModel.addElement(a);
        }
        
        this.jListAutor.setModel(defaultListModel);
    }

    public void pegarClasse(Autor autor) {
        listaAutor.add(autor);
    }
    
    public void prenecherListaGenero(){
        DefaultListModel<Genero> defaultListModel = new DefaultListModel<>();
        
        for(Genero a : listaGenero){
            defaultListModel.addElement(a);
        }
        
        this.jListGenero.setModel(defaultListModel);
    }

    public void pegarClasse(Genero genero) {
        listaGenero.add(genero);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldTituloCadastro = new javax.swing.JTextField();
        jTextFieldEditoraCadastro = new javax.swing.JTextField();
        jButtonGeneroCadastroLivro = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jButtonAutorCadastroLivro = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListGenero = new javax.swing.JList();
        jScrollPane2 = new javax.swing.JScrollPane();
        jListAutor = new javax.swing.JList();
        jButtonAdicionarGenero = new javax.swing.JButton();
        jButtonAdicionarAutor = new javax.swing.JButton();
        jFormattedTextFieldAnoEdicaoCadastro = new javax.swing.JFormattedTextField();
        jFormattedTextFieldISBNCadastro1 = new javax.swing.JFormattedTextField();
        jButtonSalvarLivro = new javax.swing.JButton();
        jButtonCancelarLivro = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel1.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jLabel1.setText("Dados do Livro");

        jButtonGeneroCadastroLivro.setText("Novo Gênero");
        jButtonGeneroCadastroLivro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonGeneroCadastroLivroActionPerformed(evt);
            }
        });

        jLabel2.setText("Título:");

        jLabel3.setText("ISBN:");

        jLabel4.setText("Ano da Edição:");

        jLabel5.setText("Editora:");

        jLabel6.setText("Gênero:");

        jLabel7.setText("Autor:");

        jButtonAutorCadastroLivro.setText("Novo Autor");
        jButtonAutorCadastroLivro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAutorCadastroLivroActionPerformed(evt);
            }
        });

        jScrollPane1.setViewportView(jListGenero);

        jScrollPane2.setViewportView(jListAutor);

        jButtonAdicionarGenero.setText("Adicionar Gênero");
        jButtonAdicionarGenero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdicionarGeneroActionPerformed(evt);
            }
        });

        jButtonAdicionarAutor.setText("Adicionar Autor");
        jButtonAdicionarAutor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdicionarAutorActionPerformed(evt);
            }
        });

        try {
            jFormattedTextFieldAnoEdicaoCadastro.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        try {
            jFormattedTextFieldISBNCadastro1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButtonAdicionarGenero)
                            .addComponent(jButtonGeneroCadastroLivro)))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jFormattedTextFieldAnoEdicaoCadastro, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jTextFieldEditoraCadastro, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE)
                        .addComponent(jTextFieldTituloCadastro, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jFormattedTextFieldISBNCadastro1, javax.swing.GroupLayout.Alignment.LEADING)))
                .addContainerGap(22, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonAdicionarAutor)
                    .addComponent(jButtonAutorCadastroLivro))
                .addGap(34, 34, 34))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldTituloCadastro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jFormattedTextFieldISBNCadastro1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jFormattedTextFieldAnoEdicaoCadastro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldEditoraCadastro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButtonAdicionarGenero)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonGeneroCadastroLivro))
                    .addComponent(jLabel6)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(52, 52, 52))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jButtonAdicionarAutor)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(jButtonAutorCadastroLivro))))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        jButtonSalvarLivro.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jButtonSalvarLivro.setText("Salvar");
        jButtonSalvarLivro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarLivroActionPerformed(evt);
            }
        });

        jButtonCancelarLivro.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jButtonCancelarLivro.setText("Cancelar");
        jButtonCancelarLivro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarLivroActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButtonSalvarLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonCancelarLivro)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonCancelarLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonSalvarLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCancelarLivroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarLivroActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButtonCancelarLivroActionPerformed

    private void jButtonGeneroCadastroLivroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonGeneroCadastroLivroActionPerformed
        new JFrameGeneroCadastro();
    }//GEN-LAST:event_jButtonGeneroCadastroLivroActionPerformed

    private void jButtonAutorCadastroLivroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAutorCadastroLivroActionPerformed
        new JFrameAutorCadastro();
    }//GEN-LAST:event_jButtonAutorCadastroLivroActionPerformed

    private void jButtonAdicionarAutorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarAutorActionPerformed
        new JFrameAutorAdicionar(this);
    }//GEN-LAST:event_jButtonAdicionarAutorActionPerformed

    private void jButtonAdicionarGeneroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarGeneroActionPerformed
        new JFrameGeneroAdicionar(this);
    }//GEN-LAST:event_jButtonAdicionarGeneroActionPerformed

    private void jButtonSalvarLivroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarLivroActionPerformed
        this.salvar();
    }//GEN-LAST:event_jButtonSalvarLivroActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdicionarAutor;
    private javax.swing.JButton jButtonAdicionarGenero;
    private javax.swing.JButton jButtonAutorCadastroLivro;
    private javax.swing.JButton jButtonCancelarLivro;
    private javax.swing.JButton jButtonGeneroCadastroLivro;
    private javax.swing.JButton jButtonSalvarLivro;
    private javax.swing.JFormattedTextField jFormattedTextFieldAnoEdicaoCadastro;
    private javax.swing.JFormattedTextField jFormattedTextFieldISBNCadastro1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JList jListAutor;
    private javax.swing.JList jListGenero;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField jTextFieldEditoraCadastro;
    private javax.swing.JTextField jTextFieldTituloCadastro;
    // End of variables declaration//GEN-END:variables
}
